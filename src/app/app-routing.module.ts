import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchImagesComponent } from './components/search-images/search-images.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';


const routes: Routes = [
  { path: '', redirectTo: 'search-images', pathMatch: 'full' },
  { path: 'search-images', component: SearchImagesComponent },
  { path: 'search-results', component: SearchResultsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
