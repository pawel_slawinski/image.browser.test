import { Component, OnInit } from '@angular/core';
import { UnsplashService } from 'src/app/services/unsplash/unsplash.service';

@Component({
  selector: 'app-search-images',
  templateUrl: './search-images.component.html',
  styleUrls: ['./search-images.component.scss']
})
export class SearchImagesComponent implements OnInit {

  public randomImage: any  = [];

  constructor(private unsplashService: UnsplashService) { }

  ngOnInit() {

    this.onGetRandomPhoto();
    }

  onGetRandomPhoto() {
    this.unsplashService.getRandomPhoto()
    .subscribe((resp: any) => {
      console.log(resp);
    });
  }
}
