import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UnsplashService {

  BASE_URL = 'https://api.unsplash.com';
  RANDOM_PHOTO = '/photos/random';
  MY_AUTH_KEY = '/?client_id=Eq84HDcHnD6wR3C7PqP5Nh9GEkcvxSljbhuKNgxfap0';

  constructor(private http: HttpClient) { }

  getRandomPhoto() {
    return this.http.get(this.BASE_URL + this.RANDOM_PHOTO + this.MY_AUTH_KEY);
  }
}
